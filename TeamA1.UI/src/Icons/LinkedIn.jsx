/* eslint import/no-anonymous-default-export: [2, {"allowArrowFunction": true}] */
export default () =>
    <svg fill="currentColor" width="1.04em" height="1em" viewBox="0 0 16.842 16.099">
        <path id="Path_518" data-name="Path 518" d="M2.042-.668A1.882,1.882,0,1,0,2,3.085h.023A1.883,1.883,0,1,0,2.042-.668Zm0,0" transform="translate(0 0.668)" />
        <path id="Path_519" data-name="Path 519" d="M8.109,198.313h3.61v10.862H8.109Zm0,0" transform="translate(-7.896 -193.077)" />
        <path id="Path_520" data-name="Path 520" d="M228.079,188.625a4.523,4.523,0,0,0-3.254,1.83V188.88h-3.611v10.862h3.61v-6.066a2.474,2.474,0,0,1,.119-.881,1.976,1.976,0,0,1,1.852-1.32c1.306,0,1.829,1,1.829,2.456v5.811h3.61v-6.228c0-3.336-1.781-4.889-4.156-4.889Zm0,0" transform="translate(-215.394 -183.644)" />
    </svg>
