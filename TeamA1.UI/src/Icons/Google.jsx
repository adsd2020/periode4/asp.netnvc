/* eslint import/no-anonymous-default-export: [2, {"allowArrowFunction": true}] */
export default () => 
    <svg fill="currentColor" width="0.98em" height="1em" viewBox="0 0 15.781 16.099">
        <path id="path" d="M107.039,101.55h7.583a9.185,9.185,0,0,1,.159,1.687,7.7,7.7,0,0,1-7.741,7.861,8.049,8.049,0,0,1,0-16.1,7.817,7.817,0,0,1,5.4,2.1l-2.283,2.263h0a4.414,4.414,0,0,0-3.116-1.211,4.9,4.9,0,0,0,0,9.806,4.125,4.125,0,0,0,4.387-3.275h-4.387V101.55" transform="translate(-99 -95)"/>
    </svg>