const Reducer = (state, action) => {
    switch (action.type) {
        case 'UNSET_ALL':
            return {
                ...state,
                user: undefined,
                session: null,
                toast: null,
            }
        case 'SET_USER':
            return {
                ...state,
                user: action.payload
            };
        case 'SET_SESSION':
            return {
                ...state,
                session: action.payload
            };
        case 'SET_TOAST':
            return {
                ...state,
                toast: action.payload
            };
        case 'SET_ERROR':
            try {
                return {
                    ...state,
                    toast: { message: action.payload.data.Message, status: action.payload.status, type: 'error' }
                };
            }
            catch {
                return {
                    ...state,
                    toast: { message: 'System got a malformed response. Your connection has been interrupted.', type: 'error' }
                };
            }
        default:
            return state;
    }
};

export default Reducer;