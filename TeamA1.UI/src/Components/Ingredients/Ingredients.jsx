import Checkbox from '../Checkbox/Checkbox'
import PropTypes from 'prop-types'
import './Ingredients.scss'

const Ingredients = ({ item, tags }) => {
    return (
        <div className="ingredients__container">
            <div className="ingredients__header">
                <h2 className="ingredients__heading">Ingredients</h2>
            </div>
            <div className="ingredients__content">
                {item?.Ingredients && item.Ingredients.map((item, index) => {
                    return <div className="ingredients__content-list" key={index}>
                        <Checkbox
                            isChecked={
                                tags.some(tag => tag.includes(item.Label.toLowerCase())) || 
                                tags.some(tag => item.Label.toLowerCase().includes(tag))
                            }
                        />
                        <span className="ingredients__content-list-amount">{item.Amount} {item.Unit}</span>
                        <span>{item.Label}</span>
                    </div> 
                })}
            </div>
            {item?.optional && 
            <div className="ingredients__footer">
                <h2 className="ingredients__heading">Optional</h2>
                <span>{item.Optional}</span>
            </div>}
        </div> 
    )
}

export default Ingredients

Ingredients.defaultProps = {
    item: {},   
    tags: []
}

Ingredients.propTypes = {
    item: PropTypes.object,
    tags: PropTypes.array
}