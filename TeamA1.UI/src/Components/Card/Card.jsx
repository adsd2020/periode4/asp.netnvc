import React from 'react'
import "./Card.scss";

const Card = ({ children, isPartitioned, isShrunk, isRow = false, className = '' }) => {
    return (
        <div
            className={`
                ${className}
                card__container
                card__container--${isShrunk ? 'shrunk' : 'stretched'}
                card__container--${isRow ? 'row' : 'column'}--${isPartitioned ? 'partitioned' : 'whole'}`
            }
        >
            {React.Children.map(children, Child => {
                return Child ? <div className="card__content-block">{Child}</div> : ''
            })}
        </div>
    )
}

export default Card
