import React, { useState, useEffect } from 'react'
import PropTypes from 'prop-types'
import './Input.scss'

const Input = ({ label, placeholder, value, name, min, max, step, minLength, callBack, type, className, isReadonly, isRequired, isDisabled, isShrunk, onInvalid, callBackOnChange }) => {
    const [fieldValue, setFieldValue] = useState(value ?? '')

    const resetValidity = e => e.target.setCustomValidity("")

    useEffect(() => setFieldValue(value), [value])

    const callBackFunction = e => {
        setFieldValue(e.target.value)
        callBack && callBack(e.target.value)
    }

    return (
        <label
            onBlur={e => !isReadonly && callBackFunction(e)}
            onChange={e => callBackOnChange && callBackFunction(e)}
            onInput={e => callBackOnChange && callBackFunction(e)}
            onPaste={e => callBackFunction(e)}
            className={`
                input__container 
                ${className ? className : ''}
                ${isDisabled ? 'input__container--disabled' : ''}
            `}
        >
            {label && <span className="input__label">{label}</span>}
            <input
                className={`
                input__field
                input__field--${isShrunk ? 'shrunk' : 'stretched'}
            `}
                readOnly={isReadonly}
                placeholder={placeholder}
                onInvalid={onInvalid}
                value={fieldValue ?? ''}
                required={isRequired}
                minLength={minLength}
                min={min}
                max={max}
                step={step}
                name={name}
                onChange={e => {
                    setFieldValue(e.target.value)
                    resetValidity(e)
                }}
                type={type}
            />
        </label>
    )
}

Input.propTypes = {
    label: PropTypes.string,
    type: PropTypes.string,
    callBack: PropTypes.func,
    isReadonly: PropTypes.bool,
    isRequired: PropTypes.bool,
    isDisabled: PropTypes.bool,
    isShrunk: PropTypes.bool,
    isSelect: PropTypes.object,
    isDate: PropTypes.bool
}

Input.defaultProps = {
    type: 'text'
}

export default Input

