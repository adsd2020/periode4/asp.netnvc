import React from 'react'
import "./RecipeCard.scss";
import Icons from '../Icons/Icons'
import { useHistory } from 'react-router-dom'

const RecipeCard = ({recipe, id, canClick}) => {
    let history = useHistory()

    const handleClick = () => canClick && history.push(`/recipe/${id}`)
    
    return (
        <div onClick={handleClick} className="recipe-card__container">
            <div className="recipe-card__container-image">
                <img className="recipe-card__image" src={recipe.Image} alt={recipe.Title}/>
            </div>
            <div className="recipe-card__container-content">
                <div className="recipe-card__title">{recipe.Title}</div>
                <div className="recipe-card__info">
                    <div className="recipe-card__detail"><Icons icon="time-oclock" size={18}/> <span>{recipe.PrepTime}</span></div>
                    <div className="recipe-card__detail"><Icons icon="star" size={18}/> {recipe.Rating}</div>
                </div>
            </div>
        </div>
    )
}

export default RecipeCard