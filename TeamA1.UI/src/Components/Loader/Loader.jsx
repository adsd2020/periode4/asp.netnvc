import React from "react";
import "./Loader.scss";

const Loader = ({ isLoaded }) => {
    return (
        <div className={`loader__container loader__container--${isLoaded ? 'loaded' : 'loading'}`}>
            <div className="loader__spinner" />
            <div className="loader__dot" />
            <div className="loader__circle" />
        </div>
    )
}

export default Loader;
