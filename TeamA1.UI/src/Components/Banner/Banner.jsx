import React from 'react';
import './Banner.scss'

const Banner = ({ header, children, imagePath }) =>{
    return(
        <div className="banner"> 
            <div className="banner__text">
                <h1 className="banner__heading">{header}</h1>
                {React.Children.only(<span className="banner__child">{children}</span>)}
            </div>
            <img className="banner__foto" src={imagePath} alt="food" />
        </div>
    )
}

export default Banner