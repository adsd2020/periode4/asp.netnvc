import React from 'react';
import './Icons.scss';

const Icons = ({icon, size, className}) =>
    <div style={{fontSize: size}} className={`icons__content-${icon} icons__container ${className ? className : ''}`} /> 
    

export default Icons