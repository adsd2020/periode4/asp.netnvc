﻿/*eslint unicode-bom: ["error", "always"]*/
import Header from '../Header/Header'
import Loader from '../Loader/Loader'

import './Layout.scss'

const Layout = ({children, isLoaded, blockname = ""}) => <div className={`${blockname}__container layout__container`}>
    <div className="layout__header">
        <Header/>
    </div>
    {isLoaded && <div className={
        `${blockname}__content 
        layout__content
        layout__content--${isLoaded ? 'loaded' : 'loading'}`
    }>
        {children}
    </div>}
    <Loader isLoaded={isLoaded}/>
</div>

export default Layout