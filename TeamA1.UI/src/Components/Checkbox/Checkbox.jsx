import React, { useState, useEffect } from 'react'
import './Checkbox.scss'
import Icons from '../Icons/Icons'

const Checkbox = ({ callBack, callBackParam, activeIcon, isChecked, isFunctional, isDisabled }) => {
    const [activeState, setActiveState] = useState(isChecked);

    useEffect(() => {
        setActiveState(isChecked)
    }, [isChecked])

    const toggleCheckbox = () => {
        setActiveState(!activeState)
        callBack && callBack(!activeState, callBackParam)
    }

    return (
        <div className={`checkbox__container checkbox__container--${(activeState || isFunctional) ? 'active' : 'inactive'} ${isDisabled ? 'checkbox__container--disabled' : ''}`} onClick={() => isFunctional ? callBack() : toggleCheckbox()}>
            {(activeState || isFunctional || isDisabled) && <Icons icon={activeIcon ?? 'check'} size={14} />}
        </div>
    )
}

export default Checkbox;