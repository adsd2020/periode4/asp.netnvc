import React, { useState, useRef } from "react";
import Slider from "react-slick";

import RecipeCard from '../RecipeCard/RecipeCard'

import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

import './Carousel.scss'

const Carousel = ({items}) => {
  const [canClick, setCanClick] = useState(true)
  let timer = useRef()
  
  const onDrag = () => {
    setCanClick(true)
    timer = setTimeout(() => setCanClick(false), 120)
  }

  const onDrop = () => {
    clearTimeout(timer)
    !canClick && setTimeout(() => setCanClick(true), 50)
  }

  const settings = {
    dots: false,
    arrows: false,
    infinite: true,
    speed: 100,
    slidesToShow: 3,
    swipeToSlide: true,
    centerMode: true,
    centerPadding: '300px',
    className: 'carousel__content',
    responsive: [
      {
        breakpoint: 1400,
        settings: {
          slidesToShow: 2,
        }
      },
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 1,
        }
      }
    ]
  }

  return (
    <div onMouseDown={onDrag} onMouseUp={onDrop} className="carousel__container">
      <Slider {...settings}>
        {(items && items.length) && items.map((item, index) => <RecipeCard canClick={canClick} id={item.Id} key={index} recipe={item}/>)}
      </Slider>
    </div>
  );
}

export default Carousel