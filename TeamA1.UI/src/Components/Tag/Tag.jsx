﻿/*eslint unicode-bom: ["error", "always"]*/
import './Tag.scss'

const Tag = ({ title, clickHandler, isFullClick }) => <div
    className={`tag__container tag__container--${isFullClick ? 'functional' : 'default'}`}
    onClick={() => isFullClick ? clickHandler() : {}}
>
    {title}
    {!isFullClick && <div className="tag__button" onClick={() => clickHandler()}>🞩</div>}
</div>

export default Tag