﻿/*eslint unicode-bom: ["error", "always"]*/
import React, { useState, useRef, useEffect } from 'react'
import Tag from '../Tag/Tag'
import './TagsInput.scss'

const TagsInput = ({ limit, length, callBack }) => {
    const inputRef = useRef()
    const [state, setState] = useState({
        tags: [],
        hidden: limit ? true : false
    })

    // Load cookie
    useEffect(() => {
        let cookieValue = document.cookie.split('; ')
            .find(row => row.startsWith('food_app_fridge='))

        if (cookieValue) {
            cookieValue = cookieValue.split('=')[1];
            cookieValue && setState({...state, tags: JSON.parse(cookieValue)})
        }
    }, [])

    // Save cookie
    const saveCookie = newTags => {
        let date = new Date();
        date.setTime(date.getTime() + (20 * 24 * 60 * 60 * 1000));
        const expires = "expires=" + date.toUTCString();
        document.cookie = `food_app_fridge=${JSON.stringify(newTags)};${expires};path=/`
    }

    // Handle contentEditable key press
    const handleKey = event => {
        let key = event.keyCode
        let content = event.type === 'paste' ? `${event.target.innerText} ${event.clipboardData.getData('text')}` : event.target.innerText

        // if string is to long (and no delete event) trim it
        if ((length && content.length >= length && key !== 8) || event.type === 'paste') {
            event.preventDefault()
            content = content.substring(0, length)
            event.target.innerHTML = content
            handleFocus()
        }

        // if pressed enter create tag
        if (key === 13) {
            event.preventDefault()

            if (content) {
                let newTags = [...state.tags, content.toLowerCase()]
                setState({ ...state, tags: newTags})
                inputRef.current.innerHTML = ''
                saveCookie(newTags)
            }

            return
        }

        // if pressed escape blur input
        if (key === 27) {
            inputRef.current.blur()
            return
        }
    }

    // Focus the input and place "cursor" at the end of text content
    const handleFocus = () => {
        inputRef.current.focus()
        document.execCommand('selectAll', false, null)
        document.getSelection().collapseToEnd()
    }

    // Remove a tag from the state
    const removeTag = value => {
        let newTags = [...state.tags]
        let index = newTags.findIndex(tag => tag === value)
        newTags.splice(index, 1)
        setState({ ...state, tags: newTags})
        saveCookie(newTags)
    }

    const handleSearch = e => {
        e.stopPropagation()
        inputRef.current.blur()
        state.tags.length && callBack()
    }

    return <div className="tags-input__container" onClick={() => handleFocus()}>
        <div className="tags-input__content">
            {state.tags.slice(state.hidden ? +`-${limit}` : null).map((tag, index) => <Tag key={`tag-${tag}-index-${index}`} clickHandler={() => removeTag(tag)} title={tag}/>)}
            {limit && state.tags.length > limit && <Tag clickHandler={() => setState({ ...state, hidden: !state.hidden })} title={state.hidden ? `Show ${state.tags.length - limit} more...` : 'Show less...'} isFullClick />}
            <div
                className={`tags-input__input ${!state.tags.length ? 'tags-input__input--empty' : ''}`}
                data-placeholder="Enter ingredients here"
                type="text"
                onKeyDown={e => handleKey(e)}
                onPaste={e => handleKey(e)}
                ref={inputRef}
                contentEditable
            />
        </div>
        <div className="tags-input__search-button" onClick={e => handleSearch(e) }>🔎︎</div>
    </div>
}

export default TagsInput