import React from 'react'
import PropTypes from 'prop-types'
import { Link } from "react-router-dom"

import './Button.scss'

import Icons from '../Icons/Icons'

const Button = ({ label, target, className, icon, style, theme, shape, isDisabled, isStretched, isSubmit, isExternal, hasArrow }) => {
    const isFunc = typeof target === 'function' || isSubmit
    const buttonClass = `
		button__container
		button__container--${theme}
		button__shape--${shape}
		button__container--${isStretched ? 'stretch' : 'normal'}
		button__container--${isDisabled ? 'disabled' : 'enabled'}
		${className}
	`

    return React.createElement(isFunc ? 'button' : Link,
        // Params
        {
            style: style,
            className: buttonClass,
            to: isFunc ? undefined : typeof target === 'object' ? target : { pathname: target },
            onClick: () => (isFunc && !isDisabled && !isSubmit) ? target() : {},
            type: isSubmit && 'submit',
            target: isExternal && '_blank'
        },
        // Content
        <>
            {icon}
            {label}
            {hasArrow && <Icons icon="arrow-right" />}
        </>
    )
}

export default Button;

Button.propTypes = {
    label: PropTypes.string,
    target: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.object,
        PropTypes.func
    ]),
    icon: PropTypes.shape({
        icon: PropTypes.string,
        className: PropTypes.string,
        size: PropTypes.number
    }),
    shape: PropTypes.oneOf(['square', 'rounded', 'rounded-large', 'circle', 'circle-large']),
    theme: PropTypes.oneOf(['raspberry', 'cloudy', 'onyx']),
    className: PropTypes.string,
    isDisabled: PropTypes.bool,
    isStretched: PropTypes.bool,
    isExternal: PropTypes.bool,
    hasArrow: PropTypes.bool,
}

Button.defaultProps = {
    theme: 'onyx',
    shape: 'rounded',
    className: '',
    label: ''
}