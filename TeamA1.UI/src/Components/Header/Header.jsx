import { useContext, useState } from 'react'
import { Link, NavLink, useHistory } from 'react-router-dom'
import axios from "axios";

import { API_URL } from '../../utils/env'
import { getCookie } from '../../utils/index'
import { Context } from '../../utils/Store'
import { useMediaQuery } from '../../utils/useMediaQuery'

import Logo from '../../images/Logo'
import LogoMobile from '../../images/LogoSmall'

import Icons from '../Icons/Icons'

import './Header.scss'
import Hamburger from '../Hamburger/Hamburger'

const Header = () => {
    const [state, dispatch] = useContext(Context)
    const [openMenu, setOpenMenu] = useState(false)
    const mediaQuery = useMediaQuery('(max-width: 900px)')

    let history = useHistory();

    const Logout = () => {
        axios({
            method: 'post',
            url: API_URL + '/Account/Logout',
            headers: {
                Authorization: 'Bearer ' + getCookie('food_app_token')
            }
        })
        .then(response => {
            document.cookie = `${'food_app_token'}= ; expires = Thu, 01 Jan 1970 00:00:00 GMT`
            history.push('/login')
            dispatch({ type: 'SET_SESSION', payload: null })
        })
        .catch(error => console.error(error))
    }

    return (
        <div className="header__container">
            
            {mediaQuery && <Hamburger callBack={() => setOpenMenu(!openMenu)}/>}
            <Logo className="header__logo" />
            <LogoMobile className="header__logo header__logo-mobile" />
            
            <div className={`header__content header__content--${openMenu ? 'open' : 'closed'}`}>
                <NavLink to="/home" className="header__link" activeClassName="header__link--active">
                    <Icons icon="apps" size={28}/>
                    <span className="header__text">Home</span>
                </NavLink>
                <NavLink to="/my-recipes" className="header__link" activeClassName="header__link--active">
                    <Icons icon="list" size={28}/>
                    <span className="header__text">Saved recipies</span>
                </NavLink>
                <NavLink to="/account" className="header__link" activeClassName="header__link--active">
                    <Icons icon="user" size={28}/>
                    <span className="header__text">Account</span>
                </NavLink>
                {mediaQuery && <Link className="header__logout header__link" onClick={Logout} to="/login ">
                    <Icons icon="sign-out" size={24}/>
                </Link>}
            </div>

            {state.session && !mediaQuery && <Link className="header__logout header__link" onClick={Logout} to="/login ">
                <span className="header__text">Logout</span>
                <Icons icon="sign-out" size={24}/>
            </Link>}
        </div>
    )
}

export default Header