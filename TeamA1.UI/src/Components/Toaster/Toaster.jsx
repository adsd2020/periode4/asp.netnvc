import React, { useContext, useEffect, useState } from "react";
import { Context } from '../../utils/Store'
import './Toaster.scss'

const Toaster = () => {
    const [state, dispatch] = useContext(Context);
    const [toastState, setToastState] = useState([])

    const removeToast = async (id) => {
        const index = toastState.findIndex(e => e.id === id);
        if(toastState.length-1) {toastState[toastState.length-1].ignore = true}
        toastState.splice(index, 1);
        setToastState([...toastState]);
    }

    useEffect(() => {
        if (state.toast) {
            setToastState(toastState.concat({...state.toast, id: Math.floor(Math.random() * Math.floor(10000))}))
            dispatch({ type: 'SET_TOAST', payload: null })
        }
    }, [state.toast])

    useEffect(() => {
        const interval = setInterval(() => toastState.length && removeToast(toastState[0].id), 3000);

        if (toastState.length > 5) {
            removeToast(toastState[0].id);
            clearInterval(interval);
        }
        
        return () => clearInterval(interval);
    }, [toastState.length]);

    return (
        <div className="toaster__container">
            <div className="toaster__toasts">
                {toastState.map((toast, index) => {
                    return (
                        <div onClick={() => removeToast(toast.id)} key={`toast-${toast.id}-index-${index}`} className={`toaster__toast toaster__toast--${toast.type} ${!toast.ignore && toastState[toastState.length-1].id === toast.id && 'toaster__toast--popin'}`}>
                            <span className="toaster__title">{toast.type} <span className="toaster__status">{toast.status}</span></span>
                            <span className="toaster__message">{toast.message}</span>
                        </div>
                    )
                })}
            </div>
        </div>
    )
};

export default Toaster;