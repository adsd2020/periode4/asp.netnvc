import './Hamburger.scss'

const Hamburger = ({callBack}) => <div className="hamburger__container"><input type="checkbox" onChange={callBack} /><div className="hamburger__bun"><div className="hamburger__burger"></div></div></div>

export default Hamburger