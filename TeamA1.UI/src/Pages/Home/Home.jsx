import { useContext, useEffect, useState } from 'react'
import { useHistory } from "react-router-dom"
import axios from "axios";

import { Context } from '../../utils/Store'
import { API_URL } from '../../utils/env'

import Layout from '../../Components/Layout/Layout'
import Carousel from '../../Components/Carousel/Carousel'
import TagsInput from '../../Components/TagsInput/TagsInput'
import Button from '../../Components/Button/Button'

import Pizza from '../../Icons/Pizza'
import Fruit from '../../Icons/Fruit'

import './Home.scss'

const Home = () => {
    const [state, dispatch] = useContext(Context)
    const [carouselItems, SetCarouselItems] = useState()
    let history = useHistory()

    useEffect(() => {
        state.session && 
        axios({
            method: 'get',
            url: API_URL + '/Recipes',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${state.session}`,
            }
        })
        .then(response => {
            response && SetCarouselItems(response.data)
        })
        .catch(error => dispatch({ type: 'SET_ERROR', payload: error.response }))
    }, [state.session])

    const handleSearch = () => history.push('/search')

    const handleError = () => {
        var audio = new Audio('./error_sound.mp3');
        audio.play();
        audio.volume = .3;
    }

    return <Layout isLoaded={!!carouselItems} blockname="home">
        <TagsInput limit={20} length={15} callBack={handleSearch} />
        
        <div className="home__banner"> 
            <div className="home__banner-section">
                <h1 className="home__banner-heading">A recipe for disaster</h1>
                <span className="home__banner-paragraph">Search for recipes based on what is in your fridge and easily save recipes accustomed to your liking.</span>
            </div>
            <img className="home__banner-photo" src="./LadyDelicious.png" alt="girl looking like a snack" />
        </div>

        <div className="home__carousel-section">
            <Carousel items={carouselItems}/>
        </div>

        <div className="home__category-section">
            <h1 className="home__category-header">Categories</h1>
            <div className="home__category-items" onMouseEnter={() => handleError()}>
                <div className="home__category-chungus">
                    <img src="./logo192.png" alt="wang" />
                    Not implemented 🥺
                </div>
                <Button className="home__category-button" label="All" theme="raspberry"/>
                <Button className="home__category-button home__category-button--inactive" shape="rounded" label="Pizza" icon={<Pizza />} theme="onyx"/>
                <Button className="home__category-button home__category-button--inactive" shape="rounded" label="Fruit" icon={<Fruit />} theme="onyx"/>
            </div>  
        </div>
    </Layout>
}

export default Home