import { useState, useContext } from "react";
import { useHistory } from "react-router-dom";
import axios from "axios";

import { API_URL } from '../../utils/env'
import { Context } from '../../utils/Store'

import Input from "../../Components/Input/Input";
import Button from "../../Components/Button/Button";


const Register = () => {
    /* eslint-disable no-unused-vars */
    const [state, dispatch] = useContext(Context)
    /* eslint-enable no-unused-vars */
    const [register, setRegister] = useState({})

    let history = useHistory()

    const HandleSubmit = (e) => {
        e.preventDefault()

        axios({
            method: 'post',
            url: API_URL + '/Account/Register',
            data: register,
            headers: {
                'Content-Type': 'application/json'
            }
        })
        .then(response => {
            dispatch({ type: 'SET_TOAST', payload: {message: 'Account succesfully created'} })
            history.push('/login')
        })
        .catch(error => dispatch({ type: 'SET_ERROR', payload: error.response }))
    }

    return <>
        <h1 className="user__heading">Register</h1>

        <span className="user__subheading">Or use your personal account</span>
        <form className="user__form" onSubmit={e => HandleSubmit(e)}>
            <Input callBackOnChange name="email" value={register.Email} placeholder="Username" callBack={e => setRegister({ ...register, Email: e })} />

            <Input callBackOnChange name="password" type="password" value={register.Password} placeholder="••••••••" callBack={e => setRegister({ ...register, Password: e })} />
            
            <Input callBackOnChange name="password" type="password" value={register.ConfirmPassword} placeholder="••••••••" callBack={e => setRegister({ ...register, ConfirmPassword: e })} />

            <span className="user__form-buttons">
                <Button label="Register" theme="raspberry" isSubmit />
                <Button label="Already have an account?" theme="cloudy" target="/login" />
            </span>
        </form>
    </>
}

export default Register