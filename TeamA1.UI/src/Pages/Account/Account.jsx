import { useContext, useEffect, useState } from 'react'
import axios from "axios";

import { Context } from '../../utils/Store'
import { API_URL } from '../../utils/env'

import Input from '../../Components/Input/Input'
import Card from '../../Components/Card/Card'
import Layout from '../../Components/Layout/Layout'
import Checkbox from '../../Components/Checkbox/Checkbox'
import Button from '../../Components/Button/Button'

import './Account.scss'

const Account = () => {
    const [form, setForm] = useState({})
    const [preferences, setPreferences] = useState()
    const [state, dispatch] = useContext(Context)

    useEffect(() => {
        state.session &&
            axios({
                method: 'get',
                url: API_URL + '/Account/UserInfo',
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${state.session}`,
                }
            })
                .then(response => {
                    if (response) setForm({ ...form, username: response.data.Email })
                })
                .catch(error => dispatch({ type: 'SET_ERROR', payload: error.response }));

        axios({
            method: 'get',
            url: API_URL + '/Preferences',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${state.session}`,
            }
        })
            .then(response => {
                if (response) setPreferences(response.data)
            })
            .catch(error => {
                dispatch({ type: 'SET_TOAST', payload: error.response.data.Message })
            })

    }, [state.session])

    const updatePassword = () => {
        axios({
            method: 'post',
            url: API_URL + '/Account/ChangePassword',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${state.session}`,
            },
            data: form
        })
            .catch(error => dispatch({ type: 'SET_ERROR', payload: error.response }))
    }

    const preferenceClicked = (selectedValue, preference) => {
        axios({
            method: 'post',
            url: API_URL + '/Preferences',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${state.session}`,
            },
            data: {Type: preference, selected: selectedValue}
        })
            .catch(error => dispatch({ type: 'SET_ERROR', payload: error.response }))
    }

    return <Layout isLoaded={!!(form && preferences)} blockname="account">
        <div className="account__section">
            <Card className="account__edit-card">
                <h3 className="account__heading">Personal Information</h3>
                <Input placeholder="Username" isDisabled value={form.username} />
                <Input type="password" placeholder="Old Password" value={form.OldPassword} callBack={e => setForm({ ...form, OldPassword: e })} />
                <Input type="password" placeholder="New Password" value={form.NewPassword} callBack={e => setForm({ ...form, NewPassword: e })} />
                <Input type="password" placeholder="Confirm password" value={form.ConfirmPassword} callBack={e => setForm({ ...form, ConfirmPassword: e })} />
                <Button className="account__button" label="Save changes" theme="raspberry" target={updatePassword} />
            </Card>
        </div>
        <div className="account__section">
            <Card>
                <h3 className="account__preferences-heading">Preferences</h3>
                {preferences && preferences.map(preference => {
                    return (<span key={preference.Type} className="account__preferences-item">
                        <Checkbox isChecked={preference.Selected} callBack={preferenceClicked} callBackParam={preference.Type} />
                        <span className="account__preferences-item-heading">{preference.Type}</span>
                    </span>)
                })}
            </Card>
        </div>
    </Layout>
}

export default Account