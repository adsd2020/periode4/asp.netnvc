
import { useContext, useEffect, useState } from 'react'
import axios from "axios";

import { Context } from '../../utils/Store'
import { API_URL } from '../../utils/env'

import Layout from '../../Components/Layout/Layout'
import RecipeCard from '../../Components/RecipeCard/RecipeCard'
import TagsInput from '../../Components/TagsInput/TagsInput'

import './Search.scss'

const Search = () => {
    const [state, dispatch] = useContext(Context)
    const [data, setData] = useState([])

    const handleSearch = () => {
        let searchRequest = document.cookie.split('; ').find(row => row.startsWith('food_app_fridge=')).split('=')[1]
        // console.log(searchRequest.toString().substring(2, searchRequest.length - 2).replace());
        let ingredients = searchRequest.toString().replace("[","").replace("]","").split('"').join("").split(",");

        axios({
            method: 'POST',
            url: API_URL + '/Recipes',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${state.session}`,
            },
            data: ingredients
        })
        .then(response => {
            response && setData(response.data)
        })
        .catch(error => dispatch({ type: 'SET_ERROR', payload: error.response }))
    }

    useEffect(() => { state.session && handleSearch() }, [state.session])

    return <Layout isLoaded={!!data.length} blockname="search">
        <div className="my-recipe__header">
            <TagsInput limit={20} length={15} callBack={handleSearch} />
        </div>

        {data.length
            ? data.map((item, index) => <RecipeCard key={index} id={item.Id} recipe={item} canClick={true}/>)
            : <div>You have not saved any recipes</div>}
    </Layout>
}

export default Search