import { useContext, useEffect, useState } from 'react'
import axios from "axios";

import { Context } from '../../utils/Store'
import { API_URL } from '../../utils/env'

import Layout from '../../Components/Layout/Layout'
import RecipeCard from '../../Components/RecipeCard/RecipeCard'

import './MyRecipes.scss'

const MyRecipe = () => {
    const [state, dispatch] = useContext(Context)
    const [data, setData] = useState(undefined)

    useEffect(() => {
        state.session && 
        axios({
            method: 'get',
            url: API_URL + '/Favorites',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${state.session}`,
            }
        })
        .then(response => {
            if (response) {
                setData(response.data)
            }
        })
        .catch(error => dispatch({ type: 'SET_ERROR', payload: error.response }))
    }, [state.session])

    return (
        <Layout isLoaded={!!data} blockname="my-recipe">
            <div className="my-recipe__header">
                <div className="my-recipe__header-title">My Saved Recipes</div>
            </div>
            {data && data.length
                ? data.map((item, index) => <RecipeCard key={index} id={item.RecipeId} recipe={item} canClick={true}/>)
                : <div>You have not saved any recipes</div>}
        </Layout> 
    )
}

export default MyRecipe
