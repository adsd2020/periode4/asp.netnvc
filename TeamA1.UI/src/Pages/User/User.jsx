import { useLocation } from "react-router"

import Login from '../Login/Login'
import Register from "../Register/Register"

import './User.scss'

const User = () => {
    let location = useLocation() 

    return (
        <div className="user__container">
            <div className="user__content">
                <div className="user__content-form">
                    {location.pathname === '/login' ? <Login /> : <Register />}
                </div>
                <div className="user__content-image" />
            </div>
        </div>
    )
}

export default User