import { useState, useContext } from "react";
import { useHistory } from "react-router-dom";
import axios from "axios";

import { Context } from '../../utils/Store'

import Input from "../../Components/Input/Input";
import Button from "../../Components/Button/Button";

import { TokenURL } from '../../utils/env'

const Login = () => {
    /* eslint-disable no-unused-vars */
    const [state, dispatch] = useContext(Context)
    /* eslint-enable no-unused-vars */
    const [login, setLogin] = useState({})

    let history = useHistory()

    const HandleSubmit = (e) => {
        e.preventDefault()

        const params = new URLSearchParams()
        params.append('grant_type', 'password')
        params.append('username', login.username)
        params.append('password', login.password)

        const setCookie = (cName, cValue, expDays) => {
            let date = new Date();
            date.setTime(date.getTime() + (expDays * 24 * 60 * 60 * 1000));
            const expires = "expires=" + date.toUTCString();
            document.cookie = cName + "=" + cValue + "; " + expires + "; path=/";
        }

        axios({
            method: 'post',
            url: TokenURL + '/token',
            data: params,
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        })
        .then(response => {
            setCookie('food_app_token', response.data.access_token, response.data.expires_in)
            dispatch({ type: 'SET_SESSION', payload: response.data.access_token })
            history.push('/home')
        })
        .catch(error => dispatch({ type: 'SET_ERROR', payload: error.response }))
    }

    return <>
        <h1 className="user__heading">Login</h1>

        {/* 0Auth was not implemented in this version */}
        {/* <div className="user__external-buttons">
            <Button shape="circle" icon={<Google />} />
            <Button shape="circle" icon={<LinkedIn />} />
            <Button shape="circle" icon={<Twitter />} />
            <Button shape="circle" icon={<Instagram />} />
        </div> */}

        <span className="user__subheading">Or use your personal account</span>
        <form className="user__form" onSubmit={e => HandleSubmit(e)}>
            <Input callBackOnChange name="email" value={login.username} placeholder="Username" callBack={e => setLogin({ ...login, username: e })} />

            <Input callBackOnChange name="password" type="password" value={login.password} placeholder="••••••••" callBack={e => setLogin({ ...login, password: e })} />

            <span className="user__form-buttons">
                <Button label="Log in" theme="raspberry" isSubmit />
                <Button label="Register" theme="cloudy" target="/register" />
            </span>
        </form>
    </>
}

export default Login