import { useContext, useEffect, useState} from 'react'
import axios from "axios";

import Layout from '../../Components/Layout/Layout'
import Ingredients from '../../Components/Ingredients/Ingredients'
import Button from '../../Components/Button/Button'
import Icons from '../../Components/Icons/Icons'

import { Context } from '../../utils/Store'
import { useParams } from 'react-router-dom'
import { API_URL } from '../../utils/env'

import './Recipe.scss'

const Recipe = () => {
    const [state, dispatch] = useContext(Context)
    const [pageData, setPageData] = useState({
        content: null,
        tags: null
    })
    
    let { id } = useParams();

    useEffect(() => {
        state.session && 
        axios({
            method: 'get',
            url: API_URL + `/Recipes/${id}`,
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${state.session}`,
            }
        })
        .then(response => {
            if (response) {
                response.data.Nutritions = JSON.parse(response.data.Nutritions);
                response.data.Instructions = JSON.parse(response.data.Instructions);

                let cookieValue = document.cookie.split('; ')
                .find(row => row.startsWith('food_app_fridge='))

                cookieValue = cookieValue ? cookieValue.split('=')[1] : '[]'
                setPageData({content: response.data, tags: JSON.parse(cookieValue)})
            }
        })
        .catch(error => dispatch({ type: 'SET_ERROR', payload: error.response }))
    }, [state.session])

    const returnNutrition = (data) => {
        var list = [];

        const returnItem = (item, index) => {
            return <div key={Math.random()} className="recipe__nutritions-list-item">
                <span>{item.label}</span>
                <span>{item.amount} {item.unit}</span>
            </div>
        }

        data.map((item, index) => {
            return list.push(
                item.type === 'group' 
                    ? <div key={index} className="recipe__nutritions-list-group">
                        {item.items.map(groupedItem => returnItem(groupedItem))}
                    </div>
                    : returnItem(item)
            )
        });

        return list
    }

    const addToFavorites = () => {
        axios({
            method: 'post',
            url: API_URL + '/Favorites',
            data: {RecipeId: pageData.content.Id, Rating: pageData.content.Rating, PrepTime: pageData.content.PrepTime, Image: pageData.content.Image, Title: pageData.content.Title},
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${state.session}`,
            }
        })
        .then(response => {
            // Success message
            dispatch({ type: 'SET_TOAST', payload: {message: 'Added to favorites'} })
            setPageData({...pageData, favoriteId: response.data.Id})
        })
        .catch(error => dispatch({ type: 'SET_ERROR', payload: error.response }))
    }

    const removeFromFavorites = () => {
        axios({
            method: 'delete',
            url: `${API_URL}/Favorites/${pageData.favoriteId}`,
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${state.session}`,
            }
        })
        .then(response => {
            // Success message
            dispatch({ type: 'SET_TOAST', payload: {message: 'Removed to favorites'} })
            setPageData({...pageData, favoriteId: 0})
        })
        .catch(error => dispatch({ type: 'SET_ERROR', payload: error.response }))
    }
    
    return (
        <Layout isLoaded={!!pageData.content} blockname="recipe">
            {/* eslint-disable jsx-a11y/alt-text */}
            {/* eslint-disable react/jsx-no-duplicate-props */}
            {pageData?.content?.Image && <div className="recipe__mobile-image">
                <img src={pageData.content.Image} src={pageData.content.Title}/>
            </div> }
            {/* eslint-enable react/jsx-no-duplicate-props */}
            {/* eslint-enable jsx-a11y/alt-text */}
            {pageData?.content && <div className="recipe__data">
                <div className="recipe__header">
                    <div className="recipe__header-title">
                        {pageData.content.Title}
                    </div>
                    <div className="recipe__header-button">
                        <Button shape="circle-large" theme="raspberry" target={!pageData.favoriteId ? addToFavorites : removeFromFavorites} icon={<Icons icon={!pageData.favoriteId ? 'plus' : 'cross'} size={20} />}/>
                    </div>
                </div>
                <div className="recipe__content-left">
                    <div className="recipe__content-info">
                        <span>{pageData.content.Description}</span>
                        <span>{pageData.content.Servings} servings</span>
                    </div>
                    {pageData?.content.Ingredients && pageData.content.Ingredients.length ? <div className="recipe__content-info">
                        <Ingredients item={pageData.content} tags={pageData.tags}/>
                    </div> : ''}
                    <div className="recipe__content-info">
                        <div className="recipe__nutritions-list">
                        <h2>Nutritions</h2>
                            {pageData?.content.Nutritions && returnNutrition(pageData.content.Nutritions)}
                        </div>
                    </div>
                </div>
                <div className="recipe__content-right">
                    <div className="recipe__content-image">
                        <img src={pageData.content.Image} alt={pageData.content.Title}/>
                    </div>
                    <div className="recipe__content-info">
                        <ol>{pageData?.content.Instructions && pageData.content.Instructions.map((item, index) => <li key={index}>{item}</li>)}</ol>
                    </div>
                </div>
            </div> }
        </Layout> 
    )
}

export default Recipe

// {data?.nutritions && data.nutritions.map((item, index) => {
//     return (
//     <>
//         {item.type === 'group' && item.poep.map((group, index) => {
//             return <div className="recipe__nutritions-list-group" key={index}>
//                 <span>{group.label}</span>
//                 <span>{group.amount} {group.unit}</span>
//             </div>
//         })}
//         <div className="recipe__nutritions-list-item" key={index}>
//             <span>{item.label}</span>
//             <span>{item.amount} {item.unit}</span>
//         </div>
//     </>
//     )
// })}