/* eslint import/no-anonymous-default-export: [2, {"allowArrowFunction": true}] */
export default ({className}) =>
    <svg className={className} width="1.51em" height="1em" viewBox="0 0 28.89 19.04">
        <g id="Layer_2" data-name="Layer 2">
            <g id="Layer_1-2" data-name="Layer 1">
                <g id="Layer_1-3" data-name="Layer 1">
                    <path fill="var(--smoke)" d="M8.26,0V4.75a3,3,0,0,0-.66.1h0a2.82,2.82,0,0,0-1.9,1.06,5.33,5.33,0,0,0-.78,3.16V19H0V9.18C0,9.12,0,9.07,0,9V9a1.48,1.48,0,0,1,0-.21c0-.12,0-.23,0-.35a7.14,7.14,0,0,1,0-.8,9.48,9.48,0,0,1,2.2-5.32l.14-.13A8.24,8.24,0,0,1,7.12,0a6.08,6.08,0,0,1,.61,0h.53Z"/>
                    <path fill="var(--smoke)" d="M28.87,9.18V19H24V9.18a5.48,5.48,0,0,0-.84-3.34,3.88,3.88,0,0,0-5.4,0,5.34,5.34,0,0,0-.87,3.34V19H12V9.18a5.48,5.48,0,0,0-.84-3.34A3.8,3.8,0,0,0,9.92,5V.12a8.92,8.92,0,0,1,1.94.55A6.18,6.18,0,0,1,14.42,2.6,6.46,6.46,0,0,1,17,.67,8.81,8.81,0,0,1,20.41,0a8.35,8.35,0,0,1,6.15,2.31A9.36,9.36,0,0,1,28.87,9.18Z"/>
                    <path fill="var(--raspberry)" d="M8.26,0V4.84H8.07a2.44,2.44,0,0,0-.47,0h0a2.82,2.82,0,0,0-1.9,1.06,5.33,5.33,0,0,0-.78,3.16V19H0V9a1.48,1.48,0,0,1,0-.21c0-.12,0-.23,0-.35a7.14,7.14,0,0,1,0-.8,9.48,9.48,0,0,1,2.2-5.32l.14-.13A7.66,7.66,0,0,1,7.12,0a6.08,6.08,0,0,1,.61,0h.53Z"/>
                </g>
            </g>
        </g>
    </svg>