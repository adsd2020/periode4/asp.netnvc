import { useContext, useEffect } from 'react'
import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom'
import { getCookie, isAuthenticated } from './utils/index'

import User from './Pages/User/User'
import Search from './Pages/Search/Search'
import Recipe from './Pages/Recipe/Recipe'
import MyRecipes from './Pages/MyRecipes/MyRecipes'
import Home from './Pages/Home/Home'
import Account from './Pages/Account/Account'

import Toaster from './Components/Toaster/Toaster'

import { Context } from './utils/Store'

import './scss/Font.scss'
import './scss/App.scss'

const App = () => {
    const [state, dispatch] = useContext(Context)
    
    // Setting state session if exists
    useEffect(() => {
        dispatch({ type: 'SET_SESSION', payload: getCookie('food_app_token') })
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [state.session])

    return (
        <div className="App__container">
            <Toaster />
            <Router>
                <Switch>
                    <Route path="/login" component={User} />,
                    <Route path="/register" component={User} />
                    
                    {/* Protected Routes */}
                    <PrivateRoute path="/home"><Home /></PrivateRoute>
                    <PrivateRoute path="/search"><Search /></PrivateRoute>
                    <PrivateRoute path="/recipe/:id"><Recipe /></PrivateRoute>
                    <PrivateRoute path="/my-recipes"><MyRecipes /></PrivateRoute>
                    <PrivateRoute path="/Account"><Account /></PrivateRoute>

                    {/* Redirect */}
                    <Route path="*"><Redirect to="/home"/></Route>
                </Switch>
            </Router>
        </div>
    );
}

const PrivateRoute = ({ children, ...rest }) => {
    return (
        <Route {...rest} render={() => {
            return isAuthenticated('food_app_token') === true
                ? children
                : <Redirect to='/login' />
        }} />
    )
}

export default App;
