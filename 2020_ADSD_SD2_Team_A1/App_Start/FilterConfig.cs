﻿using System.Web;
using System.Web.Mvc;

namespace _2020_ADSD_SD2_Team_A1
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
