﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using _2020_ADSD_SD2_Team_A1.Migrations;

namespace _2020_ADSD_SD2_Team_A1.Data
{
    using Models;

    public class _2020_ADSD_SD2_Team_A1Context : ApplicationDbContext
    {
        // You can add custom code to this file. Changes will not be overwritten.
        // 
        // If you want Entity Framework to drop and regenerate your database
        // automatically whenever you change your model schema, please use data migrations.
        // For more information refer to the documentation:
        // http://msdn.microsoft.com/en-us/data/jj591621.aspx
    
        public _2020_ADSD_SD2_Team_A1Context()
        {
            /*
             * Database.SetInitializer(new CreateDatabaseIfNotExists<_2020_ADSD_SD2_Team_A1Context>());
             * Database.Initialize(true);
            */
            Database.Delete("_2020_ADSD_SD2_Team_A1Context");

            Database.SetInitializer(new CreateDatabaseIfNotExists<_2020_ADSD_SD2_Team_A1Context>());

            Database.SetInitializer(new MigrateDatabaseToLatestVersion<_2020_ADSD_SD2_Team_A1Context, Configuration>());
        }

        public System.Data.Entity.DbSet<_2020_ADSD_SD2_Team_A1.Models.Recipe> Recipes { get; set; }

        public System.Data.Entity.DbSet<_2020_ADSD_SD2_Team_A1.Models.Favorites> Favourites { get; set; }

        public System.Data.Entity.DbSet<_2020_ADSD_SD2_Team_A1.Models.Ingredient> Ingredients { get; set; }

    }
}
