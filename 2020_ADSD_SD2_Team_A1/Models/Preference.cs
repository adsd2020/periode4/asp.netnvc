﻿namespace _2020_ADSD_SD2_Team_A1.Models
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    /// <summary>
    /// The model of users preferences
    /// </summary>
    public class Preference
    {
        public int Id { get; set; }

        //Type attribute which will be exclude from database mapping.
        //this attribute and property 'TypeString' form a full property.
        [NotMapped]
        public PreferenceTypes Type;

        //The type of preference as string to save in the database
        [Required]
        [Column("Type")]
        public string TypeString
        {
            //The get method returns the value of Type attribute as a string
            get => Type.ToString();

            //the set method wil convert the received string to Enum PreferencesType and set Type attribute
            set => Type = (PreferenceTypes) Enum.Parse(typeof(PreferenceTypes), value);
        }

        //A boolean property to avoid deleting a record from database.
        //it works as a kind of soft delete to avoid rapid increase of Id in preferences table
        [Required]
        public bool Preferred { get; set; }

        //The foreignKey to make the relationship with users table.
        //this is a complementary property for User property
        //which is necessary for validation of the modelState
        [Required, ForeignKey("User")] 
        public int UserId { get; set; }

        //The user who preferred a PreferenceType
        public ApplicationUser User { get; set; }
    }

    /// <summary>
    /// The enum too set the property type of model 'Preference'
    /// </summary>
    public enum PreferenceTypes
    {
        Vegan = 0,
        Vegetarian = 1,
        LowFat = 2
    }
}