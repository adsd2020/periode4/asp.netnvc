﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace _2020_ADSD_SD2_Team_A1.Models
{
    public class Ingredient
    {
        public int Id { get; set; }
        [Required]
        public int RecipeId{ get; set; }
        [ForeignKey("RecipeId")]
        public virtual Recipe Recipe{ get; set; }
        [Required]
        public string Label { get; set; }
        [Required]
        public string Amount { get; set; }
        [Required]
        public string Unit { get; set; }
    }
}