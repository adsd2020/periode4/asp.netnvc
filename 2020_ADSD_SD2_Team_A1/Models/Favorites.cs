﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace _2020_ADSD_SD2_Team_A1.Models
{
    public class Favorites
    {
        public int Id { get; set; }
        [Required]
        [Index("IX_RecipeIdAndUserId", 1, IsUnique = true)]
        public int RecipeId { get; set; }
        [Index("IX_RecipeIdAndUserId", 2, IsUnique = true)]
        public int UserId { get; set; }
        [Required]
        public string Title{ get; set; }
        [Required]
        public double Rating { get; set; }
        [Required]
        public string PrepTime { get; set; }
        [Required]
        public string Image { get; set; }
    }
}