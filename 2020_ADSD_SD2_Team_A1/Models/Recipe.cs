﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace _2020_ADSD_SD2_Team_A1.Models
{
    public class RecipeString
    {
        public string ingredients { get; set; }
    }
    public class Recipe
    {
        public int Id { get; set; }
        [Required]
        public string Title { get; set; }

        public int favoriteId { get; set; }
        public List<Ingredient> Ingredients { get; set; }
        [Required]
        public string Nutritions { get; set; }
        [Required]
        public string Instructions { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        public int Servings { get; set; }
        public string Optional { get; set; }
        [Required]
        public string Image { get; set; }
        [Required]
        public double Rating { get; set; }
        [Required]
        public string PrepTime { get; set; }
        public string Tags { get; set; }
    }
}