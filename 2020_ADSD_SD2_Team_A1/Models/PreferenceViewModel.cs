﻿namespace _2020_ADSD_SD2_Team_A1.Models
{
    using System.ComponentModel.DataAnnotations;

    /// <summary>
    /// A viewModel to send with response to GET action of preferencesController.<para/>
    /// it contains a the type of preference as string and a boolean variable which is true of the logged in user preferred this type
    /// </summary>
    public class PreferenceViewModel
    {
        [Required] public string Type { get; set; }

        [Required] public bool Selected { get; set; }
    }
}