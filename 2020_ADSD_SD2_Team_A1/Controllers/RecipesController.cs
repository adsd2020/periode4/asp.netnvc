﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using _2020_ADSD_SD2_Team_A1.Data;
using _2020_ADSD_SD2_Team_A1.Models;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;

namespace _2020_ADSD_SD2_Team_A1.Controllers
{
    using System.Web.Mvc;

    public class RecipesController : ApiController
    {
        private _2020_ADSD_SD2_Team_A1Context db = new _2020_ADSD_SD2_Team_A1Context();

        // GET: api/Recipes
        public IHttpActionResult GetRecipes()
        {
            db.Configuration.ProxyCreationEnabled = false;
            var recipe = db.Recipes.Include("Ingredients").ToList();

            if (!recipe.Any())
                return NotFound();

            return Ok(recipe);
        }

        // GET: api/Recipes/5
        [ResponseType(typeof(Recipe))]
        public async Task<IHttpActionResult> GetRecipe(int id)
        {
            // Get the Recipe by the requested ID and add ingredients to the entry
            Recipe recipe = db.Recipes.Include("Ingredients").SingleOrDefault(x => x.Id == id);

            // Check if the entry is a favorite of the request User
            int userId = User.Identity.GetUserId<int>();
            var isFavorite = await db.Favourites
                .Where(favorite => favorite.UserId == userId)
                .Where(favorite => favorite.RecipeId == recipe.Id)
                .Select(favorite => favorite.Id)
                .FirstOrDefaultAsync();

            recipe.favoriteId = isFavorite;

            // If the recipeId is zero return Not Found
            if (recipe.Id == 0)
            {
                return NotFound();
            }

            return Ok(recipe);
        }

        // PUT: api/Recipes/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutRecipe(int id, Recipe recipe)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != recipe.Id)
            {
                return BadRequest();
            }

            db.Entry(recipe).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!RecipeExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Recipes
        // [HTTPPost]
        [ResponseType(typeof(Recipe))]
        public async Task<IHttpActionResult> PostRecipe([FromBody] string[] ingredients)
        {
            // Make a list of ingredients from the requested data
            var tags = ingredients.Select(x => x.Trim()).ToList();

            // Get recipes and compare the ingredients to the ingredients of each recipe 
            // in the database if an ingredient matches add it to the list.
            var recipes = await db.Recipes
                .Include(R=>R.Ingredients)
                .Where(R=>tags.All(t=>
                    R.Ingredients.Select(l=>l.Label)
                    .ToList()
                    .Contains(t)
                ))
                .ToListAsync();

            return Ok(recipes);
        }

        // DELETE: api/Recipes/5
        [ResponseType(typeof(Recipe))]
        public async Task<IHttpActionResult> DeleteRecipe(int id)
        {
            Recipe recipe = await db.Recipes.FindAsync(id);
            if (recipe == null)
            {
                return NotFound();
            }

            db.Recipes.Remove(recipe);
            await db.SaveChangesAsync();

            return Ok(recipe);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool RecipeExists(int id)
        {
            return db.Recipes.Count(e => e.Id == id) > 0;
        }
    }
}