﻿namespace _2020_ADSD_SD2_Team_A1.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Http;
    using Microsoft.AspNet.Identity;
    using Models;

    /// <summary>
    /// Controller to handle the crud request for preferences<para/>
    /// <inheritdoc cref="ApiController"/>
    /// </summary>
    public class PreferencesController : ApiController
    {
        //Private property of the type DbContext
        private ApplicationDbContext Db { get; }

        /// <summary>
        /// Constructor of the class<para/>
        /// It set the dbContext property
        /// </summary>
        public PreferencesController()
        {
            Db = new ApplicationDbContext();
        }

        /// <summary>
        /// Dispose the dbContext of this controller at the end of the lifecycle of this class
        /// </summary>
        /// <param name="disposing"></param>
        protected override void Dispose(bool disposing)
        {
            Db.Dispose();
            base.Dispose(disposing);
        }

        /// <summary>
        /// Get action to handle if get requests without the id parameter.<para/>
        /// request uri: api/preferences
        /// </summary>
        /// <returns>
        /// IHttpResult that is a Interface to response to Api requests
        /// </returns>
        public IHttpActionResult Get()
        {
            // Get the Id of loggedIn user as integer.
            var loggedInUser = User.Identity.GetUserId<int>();

            //Check if the id of logged in user is zero which the default value of integer is
            //it means that no user logged in or went something wrong by get userId.
            //in that case return a bad request response
            if (loggedInUser == 0)
                return BadRequest("Login first");

            //Pick up all preferences of the logged in user from database and put it into a list of preferences.
            var userPreferences = Db.Preferences.Where(p => p.UserId == loggedInUser).ToList();

            //Make a empty list to collect preferenceViewModel to send them with the response
            var viewModels = new List<PreferenceViewModel>();

            //Loop through all values in PreferenceTypes enum to send them all with response and their selected state.
            foreach (var preferenceName in Enum.GetNames(typeof(PreferenceTypes)))
            {
                // make an instance of PreferenceViewModel and set their properties
                var viewModel = new PreferenceViewModel
                {
                    //set the name of the current enum value in the loop into property type of viewmodel.
                    Type = preferenceName,

                    //Run a linq extension method on the list of preferences, which earlier picked up from database to figure out if the user preferred this type.
                    Selected = userPreferences.Where(p => p.Preferred).Select(p => p.TypeString)
                        .Contains(preferenceName)
                };

                //Add the made viewModel to the earlier defined list, which will send to the requested client.
                viewModels.Add(viewModel);
            }

            //Make ans send a 200 response using helper function.
            return Ok(viewModels);
        }

        /// <summary>
        /// Post action to Save and edit in Preferences table.<para/>
        /// request uri: api/preferences
        /// </summary>
        /// <param name="preferenceViewModel">From the body of request bind to PreferenceViewModel</param>
        /// <returns>
        /// IHttpResult that is a Interface to response to Api requests
        /// </returns>
        [HttpPost]
        public IHttpActionResult PostPreference(PreferenceViewModel preferenceViewModel)
        {
            //Check if the ModelState is invalid, send a response 400-BadRequest with a message
            if (!ModelState.IsValid)
                return BadRequest("Something went wrong, refresh the page and try it again!");

            // Get the Id of loggedIn user as integer.
            var loggedInUser = User.Identity.GetUserId<int>();

            //Check if the id of logged in user is zero which the default value of integer is
            //it means that no user logged in or went something wrong by get userId.
            //in that case return a bad request response
            if (loggedInUser == 0)
                return BadRequest("Login first");

            //Try to pickup the first record from preferences table where its userId is equal to id of logged in user
            //and its type is equal to the type of received type via request body and if it does not return a preference model (Will be null)
            //then make a new instance of Preference model.
            var preference =
                Db.Preferences.FirstOrDefault(p => p.UserId == loggedInUser && p.TypeString == preferenceViewModel.Type) ?? new Preference();

            //Set the properties of preference model
            preference.UserId = loggedInUser;
            preference.TypeString = preferenceViewModel.Type;
            preference.Preferred = preferenceViewModel.Selected;

            //If the id of preference model is zero it means that it a new record is and must be add to the DbSet
            if (preference.Id == 0)
                Db.Preferences.Add(preference);

            //Save the changes in the database.
            Db.SaveChanges();

            //send a 200-OK response back
            return Ok();
        }
    }
}