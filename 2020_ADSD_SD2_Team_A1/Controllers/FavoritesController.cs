﻿using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.ValueProviders;
using _2020_ADSD_SD2_Team_A1.Data;
using _2020_ADSD_SD2_Team_A1.Models;
using Microsoft.AspNet.Identity;
namespace _2020_ADSD_SD2_Team_A1.Controllers
{
    [Authorize]
    public class FavoritesController : ApiController
    {
        private _2020_ADSD_SD2_Team_A1Context db = new _2020_ADSD_SD2_Team_A1Context();

        // GET: api/Favorites
        public IHttpActionResult GetFavorites()
        {
            int userid = User.Identity.GetUserId<int>();

            // Take the UserId from the request and use it to get the favorite entries
            IQueryable<Favorites> favorites = db.Favourites.Where(p => p.UserId == userid).Select(p => p);
           
            // Return with all favorite entries
            return Ok(favorites);
        }

        // PUT: api/Favourites/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutFavorites(int id, Favorites favourites)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            if (id != favourites.Id)
                return BadRequest();

            db.Entry(favourites).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FavoritesExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Favorites
        [ResponseType(typeof(Favorites))]
        public async Task<IHttpActionResult> PostFavorites([FromBody] Favorites favorites)
        {
            // Creating a new favorites entry with the data from the request body
            favorites = new Favorites { UserId = User.Identity.GetUserId<int>(), RecipeId = favorites.RecipeId, Image = favorites.Image, PrepTime = favorites.PrepTime, Rating = favorites.Rating, Title = favorites.Title};
            // Add userId to the entry from the token that has been sent
            favorites.UserId = User.Identity.GetUserId<int>();
            
            // If the new entry is NOT valid send a bad request
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            db.Favourites.Add(favorites);
            await db.SaveChangesAsync();

            // Return FavoriteId to frontend
            return CreatedAtRoute("DefaultApi", new { id = favorites.Id }, favorites);
        }

        // DELETE: api/Favourites/5
        [ResponseType(typeof(Favorites))]
        public async Task<IHttpActionResult> DeleteFavorites(int id)
        {
            Favorites favorites = await db.Favourites.FindAsync(id);
            if (favorites == null)
                return NotFound();

            db.Favourites.Remove(favorites);
            await db.SaveChangesAsync();

            return Ok(favorites);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool FavoritesExists(int id)
        {
            return db.Favourites.Count(e => e.Id == id) > 0;
        }
    }
}