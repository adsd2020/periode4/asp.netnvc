namespace _2020_ADSD_SD2_Team_A1.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Tables_typo_correction : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.Favourites", "IX_RecipeIdAndUserId");
            CreateTable(
                "dbo.Favorites",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        RecipeId = c.Int(nullable: false),
                        UserId = c.Int(nullable: false),
                        Title = c.String(nullable: false),
                        Rating = c.Double(nullable: false),
                        PrepTime = c.String(nullable: false),
                        Image = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => new { t.RecipeId, t.UserId }, unique: true, name: "IX_RecipeIdAndUserId");
            
            AddColumn("dbo.Recipes", "favoriteId", c => c.Int(nullable: false));
            AddColumn("dbo.Recipes", "PrepTime", c => c.String(nullable: false));
            DropColumn("dbo.Recipes", "favourite_id");
            DropColumn("dbo.Recipes", "Prep_time");
            DropTable("dbo.Favourites");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Favourites",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Recipe_id = c.Int(nullable: false),
                        User_id = c.Int(nullable: false),
                        Title = c.String(nullable: false),
                        Rating = c.Double(nullable: false),
                        Prep_time = c.String(nullable: false),
                        Image = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Recipes", "Prep_time", c => c.String(nullable: false));
            AddColumn("dbo.Recipes", "favourite_id", c => c.Int(nullable: false));
            DropIndex("dbo.Favorites", "IX_RecipeIdAndUserId");
            DropColumn("dbo.Recipes", "PrepTime");
            DropColumn("dbo.Recipes", "favoriteId");
            DropTable("dbo.Favorites");
            CreateIndex("dbo.Favourites", new[] { "Recipe_id", "User_id" }, unique: true, name: "IX_RecipeIdAndUserId");
        }
    }
}
