// <auto-generated />
namespace _2020_ADSD_SD2_Team_A1.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class Tables_typo_correction : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(Tables_typo_correction));
        
        string IMigrationMetadata.Id
        {
            get { return "202106031543579_Tables_typo_correction"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return Resources.GetString("Source"); }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
