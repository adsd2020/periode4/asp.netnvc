namespace _2020_ADSD_SD2_Team_A1.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using Models;

    internal sealed class Configuration : DbMigrationsConfiguration<_2020_ADSD_SD2_Team_A1.Data._2020_ADSD_SD2_Team_A1Context>
    {
        public Configuration()
        {
            AutomaticMigrationDataLossAllowed = false;
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(_2020_ADSD_SD2_Team_A1.Data._2020_ADSD_SD2_Team_A1Context context)
        {
            //  This method will be called after migrating to the latest version.
            context.Recipes.AddOrUpdate(
                 new Recipe
                 {
                     Id = 1,
                     Title = "BBQ Chicken Pizza",
                     Tags = "[\"olive oil\", \"olive\", \"pizza dough\", \"dough\", \"barbecue sauce\", \"sauce\", \"shredded chicken\", \"chicken\", \"red onion\", \"onion\", \"shredded mozzerella\", \"mozzerella\", \"cilantro leaves\", \"cilantro\"]",
                     Nutritions = "[{ \"label\": \"energy\", \"amount\": 575, \"unit\": \"kcal\" },{ \"type\": \"group\", \"items\": [{\"label\": \"carbohydrates\", \"amount\": 39, \"unit\": \"g\" },{\"label\": \"sugars\", \"amount\": 11, \"unit\": \"g\" }]},{ \"label\": \"sodium\", \"amount\": 49, \"unit\": \"mg\" },{ \"label\": \"protein\", \"amount\": 17, \"unit\": \"g\" },{ \"type\": \"group\", \"items\": [{\"label\": \"fat\", \"amount\": 17, \"unit\": \"g\" },{\"label\": \"saturated fats\", \"amount\": 7, \"unit\": \"g\" }]},{\"label\": \"fibers\", \"amount\": 1, \"unit\": \"g\" }]",
                     Instructions = "[\"Preheat oven to 475F. Place pizza stone in the oven if using one (don't add oil to the stone) or lightly oil a baking sheet with 2 tablespoons olive oil.\", \"Roll or stretch pizza dough out into approximately a 10 to 12-inch round or an oblong shape (doesn't have to be perfect). Place dough onto a well-floured pizza peel if using a stone or onto the baking sheet.\",\"Evenly spread about 1/3 cup barbecue sauce over the pizza, keeping a 1/2-inch bare margin around the perimeter.\",\"To a medium bowl, add the chicken, remaining barbecue sauce, and toss to combine.\",\"Evenly scatter chicken over the pizza.\",\"Evenly top with red onion, mozzarella, and bake for about 8 to 13 minutes, or until cheese is melted and pizza is done to your liking.\", \"Evenly sprinkle with cilantro, slice, and serve immediately. Pizza is best warm and fresh but will keep airtight in the fridge for up to 5 days. Reheat gently if desired.\"]",
                     Description = "This amazingly fresh bbq pizza reminds me of California Pizza Kitchen's bbq chicken pizza except I think homemade is better.",
                     Servings = 1,
                     Optional = "Baking times will vary based on the dough used, how heavily you pile on the toppings, what surface you're baking on, and personal preference. Watch closely starting at about 8 minutes because it's a very hot oven and the pizza can go from raw to charred in a minute.",
                     Image = "https://www.averiecooks.com/wp-content/uploads/2016/07/bbqchickenpizza-7.jpg",
                     Rating = 5.0,
                     PrepTime = "13 minuten"
                 },
                 new Recipe
                 {
                     Id = 2,
                     Title = "Honey Mustard Chicken Skillet",
                     Tags = "[\"olive oil\", \"olive\", \"boneless chicken\", \"skinless chicken\", \"chicken breast\", \"chicken\", \"kosher salt\", \"salt\", \"black pepper\", \"pepper\", \"honey\", \"yellow mustard\", \"dijon mustard\", \"mustard\", \"corn starch\", \"starch\"]",
                     Nutritions = "[{ \"label\": \"energy\", \"amount\": 575, \"unit\": \"kcal\" },{ \"type\": \"group\", \"items\": [{\"label\": \"carbohydrates\", \"amount\": 38, \"unit\": \"g\" },{\"label\": \"sugars\", \"amount\": 35, \"unit\": \"g\" }]},{ \"label\": \"sodium\", \"amount\": 1234, \"unit\": \"mg\" },{ \"label\": \"protein\", \"amount\": 54, \"unit\": \"g\" },{ \"type\": \"group\", \"items\": [{\"label\": \"fat\", \"amount\": 21, \"unit\": \"g\" },{\"label\": \"saturated fats\", \"amount\": 4, \"unit\": \"g\" }]},{\"label\": \"fibers\", \"amount\": 1, \"unit\": \"g\" }]",
                     Instructions = "[\"To a large skillet, add the oil, chicken, evenly season with salt and pepper, and sauté for about 5 to 7 minutes, or until chicken is just cooked through; stir and flip intermittently to ensure even cooking.\", \"While chicken is sautéing, to a small bowl, add the honey, both mustards, and stir to combine. Taste and check for flavor balance and add either additional honey or mustard(s), if desired to taste.\",\"Add the honey mustard to the skillet, reduce the heat to medium-low, and stir well to combine and coat all pieces of chicken. Allow chicken to simmer in sauce for about 2 to 3 minutes, or as desired.\",\"Optionally, for a thicker and clingier sauce, add the cornstarch slurry to the skillet, stir to combine, and stir nearly constantly for 1 to 2 minutes, or until the sauce is as thick as desired. Tip - The sauce will thicken up more as it's cooled so in an effort to prevent it from becoming overly thick or getting a little gloppy when it cools, don't overdo it with the heat.\",\"Optionally garnish with parsley and serve immediately.\"]",
                     Description = "If you like dipping your chicken in honey mustard, you're going to LOVE every bite of this chicken that's coated in homemade honey mustard sauce!! Fast, EASY, juicy, tender, and made in one skillet!!",
                     Servings = 4,
                     Optional = "Storage: Chicken is best fresh but will keep airtight in the fridge for up to 5 days or in the freezer for up to 4 months.",
                     Image = "https://www.averiecooks.com/wp-content/uploads/2021/01/honeymustardchicken-4.jpg",
                     Rating = 4.3,
                     PrepTime = "15 minuten"
                 },
                 new Recipe
                 {
                     Id = 3,
                     Title = "Baked Tortilla Chips",
                     Tags = "[\"avocado oil\", \"avocado\", \"garlic powder\", \"garlic\", \"chili powder\", \"chili\", \"salt\", \"corn tortillas\", \"tortillas\"]",
                     Nutritions = "[{ \"label\": \"energy\", \"amount\": 575, \"unit\": \"kcal\" },{ \"type\": \"group\", \"items\": [{\"label\": \"carbohydrates\", \"amount\": 55, \"unit\": \"g\" },{\"label\": \"sugars\", \"amount\": 0, \"unit\": \"g\" }]},{ \"label\": \"sodium\", \"amount\": 748, \"unit\": \"mg\" },{ \"label\": \"protein\", \"amount\": 9, \"unit\": \"g\" },{ \"type\": \"group\", \"items\": [{\"label\": \"fat\", \"amount\": 14, \"unit\": \"g\" },{\"label\": \"saturated fats\", \"amount\": 3, \"unit\": \"g\" }]},{\"label\": \"fibers\", \"amount\": 3, \"unit\": \"g\" }]",
                     Instructions = "[\"Preheat oven to 400F and spray a half sheet pan with cooking spray or line with parchment paper or a Silpat; set aside.\", \"To a large bowl, whisk together the oil with the garlic and chipotle chili powders and the salt.\",\"Add the tortillas and use your hands to very gently toss to coat evenly.\",\"Arrange the tortillas on the prepared baking sheet, taking care to prevent them from touching and overlapping. A little overlapping is fine, but too much and they won't bake evenly and crisp up.\",\"Bake for about 10 minutes on the first side, flip, and bake for about 8 minutes on the second side, or until done. All ovens, tortillas, the oil used, the sheet pan, etc. vary so bake until done in your oven, making sure to keep a close eye on them so they don't burn.\", \"Allow to cool and before serving. Optionally, garnish with additional salt if desired and to taste. Chips will keep airtight at room temp for up to 5 days.\"]",
                     Description = "Baked Tortilla Chips - Making tortilla chips at home is FAST and EASY! Ready in just 20 minutes. These are BAKED not fried so they're healthier, too! Perfect for salsa, guac, or anytime you need tortilla chips!",
                     Servings = 4,
                     Optional = "The sky is really the limit when it comes to homemade tortilla chips.",
                     Image = "https://www.averiecooks.com/wp-content/uploads/2021/04/tortillachips-8.jpg",
                     Rating = 5.0,
                     PrepTime = "23 minuten"
                 },
                new Recipe
                {
                    Id = 4,
                    Title = "Tomato and Mozzarella Pesto Pasta Salad",
                    Tags = "[\"bowtie pasta\", \"pasta\", \"cherry tomatoes\", \"tomatoes\", \"mozzarella cheese balls\", \"mozzarella\", \"pesto\", \"grated parmesan\", \"parmesan\", \"black pepper\", \"pepper\", \"basil leaves\", \"basil\"]",
                    Nutritions = "[{ \"label\": \"energy\", \"amount\": 575, \"unit\": \"kcal\" },{ \"type\": \"group\", \"items\": [{\"label\": \"carbohydrates\", \"amount\": 25, \"unit\": \"g\" },{\"label\": \"sugars\", \"amount\": 8, \"unit\": \"g\" }]},{ \"label\": \"sodium\", \"amount\": 404, \"unit\": \"mg\" },{ \"label\": \"protein\", \"amount\": 13, \"unit\": \"g\" },{ \"type\": \"group\", \"items\": [{\"label\": \"fat\", \"amount\": 10, \"unit\": \"g\" },{\"label\": \"saturated fats\", \"amount\": 6, \"unit\": \"g\" }]},{\"label\": \"fibers\", \"amount\": 2, \"unit\": \"g\" }]",
                    Instructions = "[\"Cook pasta according to package directions. Drain and add to a large bowl. Note - If you don't want the mozzarella in step two to melt at all and want it to remain intact in balls, wait about 10 minutes for the pasta to cool a bit before proceeding.\", \"Add the cherry tomatoes, mozzarella, pesto, and stir to combine and coat evenly.\",\"Add the Parmesan, pepper, and stir to combine and coat evenly. Taste and if desired, add salt but I did not find it necessary.\",\"Optionally garnish with basil and serve immediately if desired or chill before serving.\"]",
                    Description = "This pesto pasa salad features tender pasta, cherry tomatoes, and fresh mozzarella coated in Parmesan cheese and DELICIOUS pesto sauce!!",
                    Servings = 8,
                    Optional = "Pasta salad will keep airtight in the fridge for up to 5 days.",
                    Image = "https://www.averiecooks.com/wp-content/uploads/2019/07/pestopastasalad-5.jpg",
                    Rating = 4.8,
                    PrepTime = "15 minuten"
                },
                new Recipe
                {
                    Id = 5,
                    Title = "Smurf ice cream",
                    Tags = "[\"milk\", \"egg yolk\", \"egg\", \"sugar\", \"maizena\", \"vanilla extract\", \"vanilla\", \"blue dye\", \"dye\", \"whipped cream\", \"cream\"]",
                    Nutritions = "[{\"label\":\"energy\",\"amount\":\"0.939\",\"unit\":\"kcal\"},{\"type\":\"group\",\"items\":[{\"label\":\"carbohydrates\",\"amount\":\"141.8\",\"unit\":\"g\"},{\"label\":\"sugars\",\"amount\":\"120.6\",\"unit\":\"g\"}]},{\"label\":\"sodium\",\"amount\":\"226.8\",\"unit\":\"mg\"},{\"label\":\"protein\",\"amount\":\"16.206\",\"unit\":\"g\"},{\"type\":\"group\",\"items\":[{\"label\":\"fat\",\"amount\":\"31.02\",\"unit\":\"g\"},{\"label\":\"saturated fats\",\"amount\":\"12.4\",\"unit\":\"g\"}]},{\"label\":\"fibers\",\"amount\":\"0.18\",\"unit\":\"g\"}]",
                    Instructions = "[\"Beat the egg together with half of the sugar. Make sure the sugar get dissolved well.\",\"Bring 1.5dl of milk to a boil.\",\"Mix the heated milk with the sugar and egg.\",\"Put the mixture in a pan on a low fire and stir until the mixture starts binding. Then move the mixture to a bowl.\",\"Mix the rest of the sugar with the rest of the milk in a new pan. Add the maizena and a dash of water. Stir it while boiling until the sugar has dissolved and then let it boil for 2 minutes.\",\"Beat the two mixtures together and stir them thoroughly until the mixture has cooled.\",\"Add a small amount of blue dye and all of the vanilla extract and mix. Make sure the blue color is a little on the dark side because the whipped cream will lighten the mixture. Then place the mixture in the fridge and let it cool\",\"When the mixture has cooled beat the whipped cream semi-rigid and add it to the mixture. Then either freeze it for around an hour or for a better result have it processed in an ice-cream machine.\"]",
                    Description = "A delicious ice cream with the flavour of? Smurfs! This recipe is filled with sugary deliciousness.",
                    Servings = 4,
                    Optional = "Alternatives to vanilla extract can be used to chance the flavour",
                    Image = "https://media02.stockfood.com/largepreviews/NDAxODI4NjY1/12962215-Smurf-ice-cream-in-a-waffle-cone.jpg",
                    Rating = 4,
                    PrepTime = "100 minutes/2 hours.",
                },
                new Recipe
                {
                    Id = 6,
                    Title = "Ham & bananas hollandaise",
                    Tags = "[\"banana\", \"lemon juice\", \"lemon\", \"boiled ham\", \"ham\", \"mustard\", \"hollandaise\", \"light cream\", \"cream\"]",
                    Nutritions = "[{\"label\":\"energy\",\"amount\":\"0.726\",\"unit\":\"kcal\"},{\"type\":\"group\",\"items\":[{\"label\":\"carbohydrates\",\"amount\":\"186\",\"unit\":\"g\"},{\"label\":\"sugars\",\"amount\":\"102\",\"unit\":\"g\"}]},{\"label\":\"sodium\",\"amount\":\"8.4\",\"unit\":\"mg\"},{\"label\":\"protein\",\"amount\":\"9\",\"unit\":\"g\"},{\"type\":\"group\",\"items\":[{\"label\":\"fat\",\"amount\":\"2.4\",\"unit\":\"g\"},{\"label\":\"saturated fats\",\"amount\":\"1.2\",\"unit\":\"g\"}]},{\"label\":\"fibers\",\"amount\":\"21\",\"unit\":\"g\"}]",
                    Instructions = "[\"Preheat oven to 400F. Lightly butter 2-quart, shallow baking dish.\",\"Peel bananas. Then sprinkle each with 1/2 tablespoon lemon juice, to prevent darkening.\",\"Spread ham slices with mustard. Wrap each banana in slice of ham. Arrange in single layer in casserole. Bake 10 minutes.\",\"Meanwhile, make sauce: In small saucepan, combine sauce mix with 1 cup water, 1 tablespoon lemon juice, and cream. Heat, stirring, to boiling; pour over bananas. Bake 5 minutes longer, or until slightly golden. Nice with a green salad for brunch or lunch.\"]",
                    Description = "A meal none of the guests will forget! This delicious meal is mild to the tongue and easy to the pallet. While still having a mustard-y tinge",
                    Servings = 6,
                    Optional = "Tomatoes and carrots for some extra sweetness",
                    Image = "https://i0.wp.com/www.midcenturymenu.com/wp-content/uploads/2010/06/HB-Casserole-050-1-scaled.jpg",
                    Rating = 13,
                    PrepTime = "25 minutes",
                },
                new Recipe
                {
                    Id = 7,
                    Title = "Curry Milk",
                    Tags = "[\"milk\", \"chili powder\", \"curry powder\", \"chili\", \"curry\"]",
                    Nutritions = "[{\"label\":\"energy\",\"amount\":\"01885\",\"unit\":\"kcal\"},{\"type\":\"group\",\"items\":[{\"label\":\"carbohydrates\",\"amount\":\"24.4\",\"unit\":\"g\"},{\"label\":\"sugars\",\"amount\":\"26.7\",\"unit\":\"g\"}]},{\"label\":\"sodium\",\"amount\":\"294.3\",\"unit\":\"mg\"},{\"label\":\"protein\",\"amount\":\"13.7\",\"unit\":\"g\"},{\"type\":\"group\",\"items\":[{\"label\":\"fat\",\"amount\":\"5.3\",\"unit\":\"g\"},{\"label\":\"saturated fats\",\"amount\":\"2.45\",\"unit\":\"g\"}]},{\"label\":\"fibers\",\"amount\":\"36.4\",\"unit\":\"g\"}]",
                    Instructions = "[\"Pour the milk into a glass and slowly add curry powder while blending with an immersion blender, spoon, or a small whisk. An immersion blender or a small whisk work best because they reduce the clumping of the curry powder.\",\"Add chili powder until well blended.\",\"Serve immediately or cover and place in the refrigerator until ready to drink. Enjoy.\"]",
                    Description = "Curry milk is easy to make at home from scratch. This recipe is made with real curry powder and just a little chili for some extra spice. Both kids and adults are going to love its taste. Unlike powdered curry milk mixes that are filled with preservatives and hard-to-pronounce ingredients, you only need three simple ingredients for this curry milk recipe. You have total control over what goes into your milk and can feel better about what your family is drinking. Every aspect of this recipe can be customized. For instance, use your favorite type of milk, whether it's dairy or an alternative like almond or soy milk. By using chili powder, you can adjust the spice to taste with more or less chili. It's also easy to scale it up for a big, ready-to-drink batch in the fridge. You can even warm it up for hot curry or add a little extra flavor.",
                    Servings = 1,
                    Optional = "The chili powder is optional for an extra tinge of spice.",
                    Image = "https://i.imgflip.com/4zxgxe.jpg",
                    Rating = 4.5,
                    PrepTime = "5 minutes",
                },
                new Recipe
                {
                    Id = 8,
                    Title = "Hot orange",
                    Tags = "[\"orange\"]",
                    Nutritions = "[{\"label\":\"energy\",\"amount\":\"0.06\",\"unit\":\"kcal\"},{\"type\":\"group\",\"items\":[{\"label\":\"carbohydrates\",\"amount\":\"15.4\",\"unit\":\"g\"},{\"label\":\"sugars\",\"amount\":\"12\",\"unit\":\"g\"}]},{\"label\":\"sodium\",\"amount\":0,\"unit\":\"mg\"},{\"label\":\"protein\",\"amount\":\"1\",\"unit\":\"g\"},{\"type\":\"group\",\"items\":[{\"label\":\"fat\",\"amount\":0,\"unit\":\"g\"},{\"label\":\"saturated fats\",\"amount\":0,\"unit\":\"g\"}]},{\"label\":\"fibers\",\"amount\":\"3\",\"unit\":\"g\"}]",
                    Instructions = "[\"Take the orange and prepare a cooking pan on a high fire.\",\"Place the orange in the pan and roll it around evenly until nicely cooked.\",\"Throw the piece of trash away and order a pizza.\"]",
                    Description = "An easy to prepare quick snack for the ultimate gourmet dining experience.",
                    Servings = 1,
                    Optional = "Sugar to mellow out the flavours",
                    Image = "https://townsquare.media/site/81/files/2020/12/IMG_6412.jpg",
                    Rating = 1,
                    PrepTime = "10 minutes",
                }
            );
            context.Ingredients.AddOrUpdate(
                // Here we are seeding olive oil, this contains a lot of calories
                new Ingredient { Id = 1, RecipeId = 1, Label = "Olive oil", Amount = "2", Unit = "tbsp" },
                // Here we are seeding Dough which weight about a pound
                new Ingredient { Id = 2, RecipeId = 1, Label = "Dough", Amount = "1", Unit = "Pound" },
                // Here we are seeding Barbecue sauce which is quite sweet from taste
                new Ingredient { Id = 3, RecipeId = 1, Label = "Barbecue sauce", Amount = "3/4", Unit = "cup" },
                // Here we are seeding Chicken breast, don't eat this raw
                new Ingredient { Id = 4, RecipeId = 1, Label = "Chicken breast", Amount = "1 1/2", Unit = "cups" },
                // Here we are seeding Red onion, its quite bitter
                new Ingredient { Id = 5, RecipeId = 1, Label = "Red onion", Amount = "1/4", Unit = "union" },
                new Ingredient { Id = 6, RecipeId = 1, Label = "Mozzarella",  Amount = "2",  Unit = "cups" },
                new Ingredient { Id = 7, RecipeId = 1, Label = "Cilantro", Amount = "1/3", Unit = "cups" },

                new Ingredient { Id = 8, RecipeId = 2, Label = "Olive oil", Amount = "3 to 4", Unit = "tbsp" },
                new Ingredient { Id = 9, RecipeId = 2, Label = "Chicken breast", Amount = "1 1/2", Unit = "pound" },
                new Ingredient { Id = 10, RecipeId = 2, Label = "Salt", Amount = "1", Unit = "ts" },
                new Ingredient { Id = 11, RecipeId = 2, Label = "Black pepper", Amount = "1/2", Unit = "ts" },
                new Ingredient { Id = 12, RecipeId = 2, Label = "Honey", Amount = "1/2", Unit = "cup" },
                new Ingredient { Id = 13, RecipeId = 2, Label = "Yellow mustard",  Amount = "1/4",  Unit = "cup" },
                new Ingredient { Id = 14, RecipeId = 2, Label = "Dijon mustard", Amount = "1/4", Unit = "cup" },
                new Ingredient { Id = 15, RecipeId = 2, Label = "Corn starch", Amount = "2", Unit = "ts" },

                new Ingredient { Id = 16, RecipeId = 3, Label = "Avocado oil", Amount = "2", Unit = "tbsp" },
                new Ingredient { Id = 17, RecipeId = 3, Label = "Garlic powder", Amount = "1", Unit = "ts" },
                new Ingredient { Id = 18, RecipeId = 3, Label = "Chipotle chili powder",  Amount = "3/4",  Unit = "ts" },
                new Ingredient { Id = 19, RecipeId = 3, Label = "Salt", Amount = "1/2", Unit = "ts" },
                new Ingredient { Id = 20, RecipeId = 3, Label = "Tortillas", Amount = "8", Unit = "small" },

                new Ingredient { Id = 21, RecipeId = 4, Label = "Pasta", Amount = "12", Unit = "ounces" },
                new Ingredient { Id = 22, RecipeId = 4, Label = "Cherry tomatoes", Amount = "1", Unit = "pound" },
                new Ingredient { Id = 23, RecipeId = 4, Label = "Mozzarella",  Amount = "8",  Unit = "ounce" },
                new Ingredient { Id = 24, RecipeId = 4, Label = "Pesto", Amount = "8", Unit = "ounce" },
                new Ingredient { Id = 25, RecipeId = 4, Label = "Parmesan cheese", Amount = "1", Unit = "cup" },
                new Ingredient { Id = 26, RecipeId = 4, Label = "Black pepper", Amount = "1", Unit = "ts" },
                new Ingredient { Id = 27, RecipeId = 4, Label = "Basil", Amount = "3", Unit = "leaves" },

                new Ingredient { Id = 28, RecipeId = 5, Label = "Milk", Amount = "4", Unit = "dl" },
                new Ingredient { Id = 29, RecipeId = 5, Label = "Egg yolk", Amount = "1", Unit = "unit" },
                new Ingredient { Id = 30, RecipeId = 5, Label = "Sugar",  Amount = "100",  Unit = "g" },
                new Ingredient { Id = 31, RecipeId = 5, Label = "Maizena", Amount = "20", Unit = "g" },
                new Ingredient { Id = 32, RecipeId = 5, Label = "Vanilla extract", Amount = "3", Unit = "tbsp" },
                new Ingredient { Id = 33, RecipeId = 5, Label = "Blue Dye", Amount = "Own discretion", Unit = "/" },
                new Ingredient { Id = 34, RecipeId = 5, Label = "Whipped cream", Amount = "2", Unit = "dl" },

                new Ingredient { Id = 35, RecipeId = 6, Label = "Bananas", Amount = "6", Unit = "units" },
                new Ingredient { Id = 36, RecipeId = 6, Label = "Lemon Juice",  Amount = "1/4",  Unit = "cup" },
                new Ingredient { Id = 37, RecipeId = 6, Label = "Ham", Amount = "6", Unit = "Slices" },
                new Ingredient { Id = 38, RecipeId = 6, Label = "Mustard", Amount = "3", Unit = "tbsp" },
                new Ingredient { Id = 39, RecipeId = 6, Label = "Hollandaise sauce", Amount = "2", Unit = "envelopes" },
                new Ingredient { Id = 40, RecipeId = 6, Label = "Light cream", Amount = "1/4", Unit = "cup" },

                new Ingredient { Id = 41, RecipeId = 7, Label = "Milk", Amount = "12", Unit = "ounces" },
                new Ingredient { Id = 42, RecipeId = 7, Label = "Chili powder", Amount = "2", Unit = "tsp" },
                new Ingredient { Id = 43, RecipeId = 7, Label = "Curry powder", Amount = "1", Unit = "tbsp" },

                new Ingredient { Id = 44, RecipeId = 8, Label = "Orange", Amount = "1", Unit = "unit" }
            );
        }
    }
}
